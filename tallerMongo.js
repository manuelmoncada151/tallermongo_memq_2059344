
use tallerMongo

// ------------------------------------------------

db.createCollection("Profesor")

db.Profesor.insertMany([
    {
        nombre:'Carlos',
        apellido: 'Rodriguez',
        telefono:'3112335678',
        documento:'45454545',
        direccion:'calle 60 No. 20-90'
    },
    {
        nombre:'Maria',
        apellido: 'Quiroz',
        telefono:'36985656',
        documento:'12121212',
        direccion:'carrera 45 No 36-88'
    },
    {
        nombre:'Carmen',
        apellido: 'Quintero',
        telefono:'310456787',
        documento:'899889',
        direccion:'carrera 12 sur No 56-98'
    },
    {
        nombre:'Julio',
        apellido: 'Mendez',
        telefono:'32165478',
        documento:'747474',
        direccion:'carrera 77 sur No 9-14'
    },
    {
        nombre:'Mario',
        apellido: 'Bross',
        telefono:'369258',
        documento:'858585',
        direccion:'carrera 11 sur No 16-24'
    },
    {
        nombre:'Sara',
        apellido: 'Cardona',
        telefono:'88888',
        documento:'242424',
        direccion:'carrera 18 sur No 30-24'
    }
])

// ------------------------------------------------

db.createCollection("Curso")

db.Curso.insertMany([
    {
        piso:'1',
        grado: 'Sexto',
        documento:'601'
    },
    {
        piso:'2',
        grado: 'Septimo',
        documento:'701'
    },
    {
        piso:'3',
        grado: 'Octavo',
        documento:'801'
    },
    {
        Piso:'4',
        grado: 'Noveno',
        documento:'901'
    },
    {
        piso:'5',
        grado: 'Decimo',
        documento:'1001'
    },
    {
        piso:'6',
        grado: 'Once',
        documento:'1101'
    }
])

// ---------------------------------------------------

db.createCollection("Estudiante")

db.Estudiante.insertMany([
    {
        nombre: 'Simon',
        apellido: 'Gonzales',
        correo: 'Simon@gmail.com',
        edad:'11'
    },
    {
        nombre: 'Sonia',
        apellido: 'Perez',
        correo: 'Sonia@gmail.com',
        edad:'12'
    },
    {
        nombre: 'Pedro',
        apellido: 'Tinjacá',
        correo: 'Pedro@gmail.com',
        edad:'13'
    },
    {
        nombre: 'Diego',
        apellido: 'Martinez',
        correo: 'Diego@gmail.com',
        edad:'14'
    },
    {
        nombre: 'Juliana',
        apellido: 'Ochoa',
        correo: 'Juliana@gmail.com',
        edad:'15'
    },
    {
        nombre: 'Ana',
        apellido: 'Cuadros',
        correo: 'Ana@gmail.com',
        edad:'16'
    }
])

// ---------------------------------------------------

db.createCollection("EstudianteCurso")

db.EstudianteCurso.insertMany([
    {
        curso: new ObjectId("5fb2ba2fd406f0dd47957bb1"),
        estudiante: new ObjectId("5fb2baadd406f0dd47957bb7")
    },
    {
        curso: new ObjectId("5fb2ba2fd406f0dd47957bb2"),
        estudiante: new ObjectId("5fb2baadd406f0dd47957bb8")
    },
    {
        curso: new ObjectId("5fb2ba2fd406f0dd47957bb3"),
        estudiante: new ObjectId("5fb2baadd406f0dd47957bb9")
    },
    {
        curso: new ObjectId("5fb2ba2fd406f0dd47957bb4"),
        estudiante: new ObjectId("5fb2baadd406f0dd47957bba")
    },
    {
        curso: new ObjectId("5fb2ba2fd406f0dd47957bb5"),
        estudiante: new ObjectId("5fb2baadd406f0dd47957bbb")
    },
    {
        curso: new ObjectId("5fb2ba2fd406f0dd47957bb6"),
        estudiante: new ObjectId("5fb2baadd406f0dd47957bbc")
    }
])

// ---------------------------------------------------

db.createCollection("ProfesorCurso")

db.ProfesorCurso.insertMany([
    {
        curso: new ObjectId("5fb2ba2fd406f0dd47957bb1"),
        profesor: new ObjectId("5fb2ba07d406f0dd47957bab")
    },
    {
        curso: new ObjectId("5fb2ba2fd406f0dd47957bb2"),
        profesor: new ObjectId("5fb2ba07d406f0dd47957bac")
    },
    {
        curso: new ObjectId("5fb2ba2fd406f0dd47957bb3"),
        profesor: new ObjectId("5fb2ba07d406f0dd47957bad")
    },
    {
        curso: new ObjectId("5fb2ba2fd406f0dd47957bb4"),
        profesor: new ObjectId("5fb2ba07d406f0dd47957bae")
    },
    {
        curso: new ObjectId("5fb2ba2fd406f0dd47957bb5"),
        profesor: new ObjectId("5fb2ba07d406f0dd47957baf")
    },
    {
        curso: new ObjectId("5fb2ba2fd406f0dd47957bb6"),
        profesor: new ObjectId("5fb2ba07d406f0dd47957bb0")
    }
])

// ---------------------------------------------------

db.Estudiante.aggregate(
    [
        {
            $match:{
                _id: ObjectId("5fb2baadd406f0dd47957bb7"),
            }
        },
        {
            $lookup:{
                from:'Estudiante',
                localField:'_id',
                foreignField:'Curso',
                as:'CursoEstudiante'
            }
        }, 
        {
            $unwind: '$CursoEstudiante'
        },
        {
            $project:{
                nombreEstudiante:'$nombre',
                apellidoEstudiante:'$apellido',
                apellidoEstudiante:'$edad',
                Correo:'$correo'
            }
        }

    ]
    ).pretty()

    // ---------------------------------------------------

db.Profesor.aggregate(
    [
        {
            $match:{
                _id: ObjectId("5fb2ba07d406f0dd47957bab"),
            }
        },
        {
            $lookup:{
                from:'Profesor',
                localField:'_id',
                foreignField:'Curso',
                as:'ProfesorCurso'
            }
        }, 
        {
            $unwind: '$ProfesorCurso'
        },
        {
            $project:{
                nombreProfesor:'$nombre',
                apellidoProfesor:'$apellido',
                Correo:'$documento'
            }
        }

    ]
    ).pretty()